// +build gofuzz

package bewerbung_latex_golang

func Fuzz(data []byte) int {
	if _, err := NewConfig(string(data)); err != nil {
		return 0
	}
	return 1
}
