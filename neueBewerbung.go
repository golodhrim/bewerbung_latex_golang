package bewerbung_latex_golang

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

// struct für Bewerber, bestehend aus Name, Vorname, Straße, PLZ, Stadt, gelernter Beruf, eMail, Telefon, Mobilnummer, Staatsangehörigkeit und Geburtsdatum.
// Das struct wird im Config-Struct benötigt.
type Bewerber struct {
	Name                string `yaml:"name"`
	Vorname             string `yaml:"vorname"`
	Straße              string `yaml:"straße"`
	Plz                 string `yaml:"plz"`
	Stadt               string `yaml:"stadt"`
	Beruf               string `yaml:"beruf"`
	Email               string `yaml:"email"`
	Telefon             string `yaml:"telefon"`
	Mobil               string `yaml:"mobil"`
	Staatsangehörigkeit string `yaml:"staatsangehörigkeit"`
	Geburtsdatum        string `yaml:"geburtsdatum"`
}

// struct-slice für Anhänge bestehend aus Anlagen
// struct-slice wird im Config-Struct benötigt
type Anhang []struct {
	Anlage string `yaml:"anlage"`
}

// Config-Struct, welches die Bewerberdaten und die Anhänge vereint und aus einer YAML Vorlage gefüttert wird.
type Config struct {
	Bewerber Bewerber `yaml:"bewerber"`
	Anhang   Anhang   `yaml:"anhang"`
}

// Funktion, welche die YAML Vorlage einließt und die Werte zur weiteren Verarbeitung in das Config-Struct speichert.
func NewConfig(configPath string) (*Config, error) {
	config := &Config{}
	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	d := yaml.NewDecoder(file)
	if err := d.Decode(&config); err != nil {
		return nil, err
	}
	return config, nil
}

// Defintion der Comandline-Flags
var currentdate = time.Now()
var dateflag = flag.String("d", currentdate.Format("02.01.2006"), "Datumsstring im Format DD.MM.YYYY (default: "+currentdate.Format("02.01.2006")+")")
var firmaflag = flag.String("f", "Musterfirma", "Der Firmenname (default: Musterfirma)")
var anredeflag = flag.String("a", "Sehr geehrte Damen und Herren", "Anrede der Kontaktperson (default: Sehr geehrte Damen und Herren)")
var nameflag = flag.String("n", "", "Name der Kontaktperson(default: \"\")")
var strasseflag = flag.String("s", "Musterstraße 1", "Straße und Hausnummer der Firma (default: Musterstraße 1)")
var plzflag = flag.String("p", "12345", "PLZ der Firma (default: 12345)")
var ortflag = flag.String("o", "Musterhausen", "Ort der Firma (default: Musterhausen)")
var positionsflag = flag.String("b", "DevSecOps Engineer", "Bezeichnung der Stelle auf die sich beworben wird (default: DevSecOps Engineer)")
var rueckmeldungsflag = flag.String("r", "beworben", "Status der Bewerbung (default: beworben)")
var languageflag = flag.String("l", "de", "Sprachauswahlstring, wechsel zwischen deutscher (de), englischer (en) oder multilingualer (both) Bewerbungsvorlage (default: de)")
var compileflag = flag.Bool("c", false, "Compile die TeX-Dateien, benötigt -f mit dem Firmennamen (default: false)")
var argeflag = flag.Bool("t", false, "Erstelle die Tabelle der Eigenbemühungen für die Arbeitsagentur (default: false)")
var configinc []string

// Liest die CSV-Datei mit den Anschriften ein und speichert sie in einem Slice aus String-Slices
func csvReader(csvPath string) ([][]string, error) {
	recordFile, err := os.Open(csvPath)
	if err != nil {
		return nil, err
	}
	defer recordFile.Close()

	reader := csv.NewReader(recordFile)

	records, _ := reader.ReadAll()

	return records, nil
}

// Schreibt die CSV-Datei neu mit dem hinzuzufügenden Eintrag für die neue Firma
func csvWriter(csvPath string, anschriftsarray [][]string) error {
	writeFile, err := os.Create(csvPath)
	if err != nil {
		return err
	}
	defer writeFile.Close()

	writer := csv.NewWriter(writeFile)

	err = writer.WriteAll(anschriftsarray)
	if err != nil {
		return err
	}
	return nil
}

// Generiert die config-Datei für den jeweiligen Bewerbungsvorgang
func createConfigContent(bcfg *Config, anschriften [][]string) []string {
	var anhang string = ""
	for _, anlage := range bcfg.Anhang {
		anhang = anhang + fmt.Sprintf("%s", anlage)[1:len(fmt.Sprintf("%s", anlage))-1] + ".\\newline "
	}
	configinc = []string{
		"%%",
		"%% \\CharacterTable",
		"%%  {Upper-case    \\A\\B\\C\\D\\E\\F\\G\\H\\I\\J\\K\\L\\M\\N\\O\\P\\Q\\R\\S\\T\\U\\V\\W\\X\\Y\\Z",
		"%%   Lower-case    \\a\\b\\c\\d\\e\\f\\g\\h\\i\\j\\k\\l\\m\\n\\o\\p\\q\\r\\s\\t\\u\\v\\w\\x\\y\\z",
		"%%   Digits        \\0\\1\\2\\3\\4\\5\\6\\7\\8\\9",
		"%%   Exclamation   \\!     Double quote  \\\"     Hash (number) \\#",
		"%%   Dollar        \\$     Percent       \\%     Ampersand     \\&",
		"%%   Acute accent  \\'     Left paren    \\(     Right paren   \\)",
		"%%   Asterisk      \\*     Plus          \\+     Comma         \\,",
		"%%   Minus         \\-     Point         \\.     Solidus       \\/",
		"%%   Colon         \\:     Semicolon     \\;     Less than     \\<",
		"%%   Equals        \\=     Greater than  \\>     Question mark \\?",
		"%%   Commercial at \\@     Left bracket  \\[     Backslash     \\\\",
		"%%   Right bracket \\]     Circumflex    \\^     Underscore    \\_",
		"%%   Grave accent  \\`     Left brace    \\{     Vertical bar  \\|",
		"%%   Right brace   \\}     Tilde         \\~}",
		"%% zusätzliche",
		"%% Farbdefinitionen:",
		"%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +",
		"%%                                                                                                 +",
		"%% Konfiguration der eigenen Daten                                                                 +",
		"%%                                                                                                 +",
		"\\Name{" + bcfg.Bewerber.Name + "}",
		"\\Vorname{" + bcfg.Bewerber.Vorname + "}",
		"\\Street{" + bcfg.Bewerber.Straße + "}",
		"\\Plz{" + bcfg.Bewerber.Plz + "}",
		"\\Stadt{" + bcfg.Bewerber.Stadt + "}",
		"\\MeinBeruf{" + bcfg.Bewerber.Beruf + "}",
		"\\EMail{" + bcfg.Bewerber.Email + "}",
		"%%\\Tel{" + bcfg.Bewerber.Telefon + "}",
		"\\Mobile{" + bcfg.Bewerber.Mobil + "}",
		"\\Sta{" + bcfg.Bewerber.Staatsangehörigkeit + "}",
		"\\GebDatum{" + bcfg.Bewerber.Geburtsdatum + "}",
		"%%                                                                                                 +",
		"%% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - +",
		"%%                                                                                                 +",
		"%% +----Achtung--------------------------------------+",
		"%% + ID meint (Zeilennummer-1) und nicht das Feld id +",
		"%% + Bsp: Eintrag in Zeile 48: ID 47                 +",
		"%% +                                                 +",
		"%% + Außer der Klasse ahilbig-bewerbung wird die     +",
		"%% + Option idPlain mitgegeben. Dann wird nach dem   +",
		"%% + exakten Match im Feld id gesucht.               +",
		"\\ID{" + strconv.Itoa(len(anschriften)-1) + "}",
		"%% +-------------------------------------------------+",
		"\\Anhang{" + anhang + "}{%",
	}
	for _, anlage := range bcfg.Anhang {
		configinc = append(configinc, "\\item "+fmt.Sprintf("%s", anlage)[1:len(fmt.Sprintf("%s", anlage))-1])
	}
	configinc = append(configinc, "}")
	configinc = append(configinc, "\\endinput")
	configinc = append(configinc, "%%")
	configinc = append(configinc, "%% End of file `config.inc'.")

	return configinc
}

// Erstellt das Slice von String-Slices welches die neue Zeile der Firma des aktuellen Bewerbungsverfahrens beinhaltet.
func createCsvEntry(csvPath string, date string, firma string, anrede string, kontakt string, strasse string, plz string, ort string, position string, rueckmeldung string) ([][]string, error) {
	anschriften, err := csvReader(csvPath)
	if err != nil {
		return nil, err
	}
	ts, _ := time.Parse("02.01.2006", date)
	_, week := ts.ISOWeek()
	var input = []string{strconv.Itoa(len(anschriften)), strconv.Itoa(week), date, firma, anrede, kontakt, strasse, plz, ort, position, rueckmeldung, ""}
	anschriften = append(anschriften, input)
	return anschriften, nil
}

// Säubert den Firmenstring für bestimmte Fälle von ungewollten Symbolen
func createApplicationLayoutTidyFirma(firma string) string {
	firma = strings.ReplaceAll(firma, " ", "_")
	firma = strings.ReplaceAll(firma, "/", "")
	firma = strings.ReplaceAll(firma, "&", "")
	firma = strings.ReplaceAll(firma, "__", "_")
	return firma
}

// Erstellt die Struktur des jeweiligen Bewerbungsverfahrens anhand der ausgewählten Sprache
func createLangContent(lang string, firma string, folder string) error {
	var nbewerbung string
	var leurocv string = folder + "eurocv.tex"
	switch lang {
	case "de":
		nbewerbung = folder + "/bewerbungsmappe-" + firma + ".tex"
		original, err := os.Open("bewerbung.tex")
		if err != nil {
			return err
		}
		new, err := os.Create(nbewerbung)
		if err != nil {
			return err
		}
		_, err = io.Copy(new, original)
		if err != nil {
			return err
		}
		err = original.Close()
		if err != nil {
			return err
		}
		err = new.Close()
		if err != nil {
			return err
		}
		return nil
	case "en":
		nbewerbung = folder + "/bewerbungsmappe-" + firma + ".tex"
		original, err := os.Open("bewerbung_en.tex")
		if err != nil {
			return err
		}
		new, err := os.Create(nbewerbung)
		if err != nil {
			return err
		}
		_, err = io.Copy(new, original)
		if err != nil {
			return err
		}
		err = original.Close()
		if err != nil {
			return err
		}
		err = new.Close()
		if err != nil {
			return err
		}
		original, err = os.Open("eurocv.tex")
		if err != nil {
			return err
		}
		new, err = os.Create(leurocv)
		if err != nil {
			return err
		}
		_, err = io.Copy(new, original)
		if err != nil {
			return err
		}
		err = original.Close()
		if err != nil {
			return err
		}
		err = new.Close()
		if err != nil {
			return err
		}
		return nil
	case "both":
		nbewerbung = folder + "/bewerbungsmappe-" + firma + ".tex"
		original, err := os.Open("bewerbung.tex")
		if err != nil {
			return err
		}
		new, err := os.Create(nbewerbung)
		if err != nil {
			return err
		}
		_, err = io.Copy(new, original)
		if err != nil {
			return err
		}
		err = original.Close()
		if err != nil {
			return err
		}
		err = new.Close()
		if err != nil {
			return err
		}
		nbewerbung = folder + "/bewerbungsmappe-" + firma + "-en.tex"
		original, err = os.Open("bewerbung_en.tex")
		if err != nil {
			return err
		}
		new, err = os.Create(nbewerbung)
		if err != nil {
			return err
		}
		_, err = io.Copy(new, original)
		if err != nil {
			return err
		}
		err = original.Close()
		if err != nil {
			return err
		}
		err = new.Close()
		if err != nil {
			return err
		}
		original, err = os.Open("eurocv.tex")
		if err != nil {
			return err
		}
		new, err = os.Create(leurocv)
		if err != nil {
			return err
		}
		_, err = io.Copy(new, original)
		if err != nil {
			return err
		}
		err = original.Close()
		if err != nil {
			return err
		}
		err = new.Close()
		if err != nil {
			return err
		}
		return nil
	default:
		return fmt.Errorf("%s is not a supported language", lang)
	}
}

// Erstellt die gemeinsame Struktur des jeweiligen Bewerbungsverfahrens. Ruft dazu auch den sprachenabhängigen Teil auf
func createApplicationLayout(firma string, language string, configinc []string) error {
	firmatidy := createApplicationLayoutTidyFirma(firma)
	var foldername string = "bewerbung-" + firmatidy
	var lanhang string = foldername + "/Anhang"
	var lfotoj string = foldername + "/Foto.jpg"
	var lfotop string = foldername + "/Foto.pdf"
	var lanschrift string = foldername + "/anschrift.csv"
	errdir := os.Mkdir(foldername, 0750)
	if errdir != nil {
		return errdir
	}
	err := createLangContent(language, firmatidy, foldername)
	if err != nil {
		return err
	}
	config, err := os.Create(foldername + "/config.inc")
	if err != nil {
		return err
	}
	configwriter := bufio.NewWriter(config)
	for _, line := range configinc {
		_, err := configwriter.WriteString(line + "\n")
		if err != nil {
			return err
		}
		err = configwriter.Flush()
		if err != nil {
			return err
		}
	}

	err = os.Symlink("../Anhang", lanhang)
	if err != nil {
		return err
	}
	err = os.Symlink("../Foto.jpg", lfotoj)
	if err != nil {
		return err
	}
	err = os.Symlink("../Foto.pdf", lfotop)
	if err != nil {
		return err
	}
	err = os.Symlink("../anschrift.csv", lanschrift)
	if err != nil {
		return err
	}
	return nil
}

// Erzeugt ein Slice mit allen gefundenen TeX-Dateien in einem Bewerbungsordner.
func findtexfiles(folder string) ([]string, error) {
	files, err := os.ReadDir(folder)
	if err != nil {
		return nil, err
	}
	var texfiles []string
	for _, file := range files {
		matched, err := regexp.MatchString(`^.*\.tex$`, file.Name())
		if err != nil {
			return nil, err
		}
		if matched {
			texfiles = append(texfiles, file.Name())
		}
	}
	return texfiles, nil
}

// compiliert die mit findtexfiles gefundenen TeX-Dateien mittels pdflatex zu pdf-Dateien
func compiletexfiles(firma string) error {
	firmatidy := createApplicationLayoutTidyFirma(firma)
	var folder string = "bewerbung-" + firmatidy
	texfiles, err := findtexfiles(folder)
	if err != nil {
		return err
	}
	fmt.Println(texfiles)
	var texoutputdirsetting string = "-output-directory=./" + folder
	for _, texfile := range texfiles {
		var texfilepath string = "./" + folder + "/" + texfile
		fmt.Println(texfilepath)
		texcmd := exec.Command("pdflatex", texoutputdirsetting, texfilepath)
		stdoutstderr, _ := texcmd.CombinedOutput()
		fmt.Printf("%s\n", stdoutstderr)
		stdoutstderr, err = texcmd.CombinedOutput()
		if err != nil {
			return err
		}
		fmt.Printf("%s\n", stdoutstderr)
	}
	return nil
}

func main() {
	cfg, err := NewConfig("./config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	flag.Parse()
	switch *compileflag {
	case false:
		switch *argeflag {
		case false:
			var csvPath string
			csvPath = "./anschrift.csv"
			anschriften, err := createCsvEntry(csvPath, *dateflag, *firmaflag, *anredeflag, *nameflag, *strasseflag, *plzflag, *ortflag, *positionsflag, *rueckmeldungsflag)
			if err != nil {
				log.Fatal(err)
			}
			err = csvWriter(csvPath, anschriften)
			if err != nil {
				log.Fatal(err)
			}
			configinc := createConfigContent(cfg, anschriften)
			err = createApplicationLayout(*firmaflag, *languageflag, configinc)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("You need to generate the output of argetabelle yourself if needed.")
			os.Exit(0)
		case true:
			var texoutputdirsetting string = "-output-directory=./"
			var texfilepath string = "./argetabelle.tex"
			texcmd := exec.Command("pdflatex", texoutputdirsetting, texfilepath)
			stdoutstderr, err := texcmd.CombinedOutput()
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", stdoutstderr)
			os.Exit(0)
		}
	case true:
		switch *argeflag {
		case false:
			err = compiletexfiles(*firmaflag)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("You need to generate the output of argetabelle yourself if needed.")
			os.Exit(0)
		case true:
			err = compiletexfiles(*firmaflag)
			if err != nil {
				log.Fatal(err)
			}
			var texoutputdirsetting string = "-output-directory=./"
			var texfilepath string = "./argetabelle.tex"
			texcmd := exec.Command("pdflatex", texoutputdirsetting, texfilepath)
			stdoutstderr, err := texcmd.CombinedOutput()
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("%s\n", stdoutstderr)
			os.Exit(0)
		}
	}
}
