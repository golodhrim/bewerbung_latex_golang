module gitlab.com/golodhrim/bewerbung_latex_golang

go 1.17

require gopkg.in/yaml.v2 v2.4.0

require gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
